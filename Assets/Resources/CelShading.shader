﻿Shader "Custom/CelShading" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf CelShadingForward

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		// This function takes the surface, the light direction, 
		half4 LightingCelShadingForward(SurfaceOutput s, half3 lightDir, half atten) {
			// Calculate the angle between the Light and the Surface Normal.
			// This is a binary diffuse calculation essentially
			half NdotL = dot(s.Normal, lightDir);
			if (NdotL <= 0.0)
				NdotL = 0;
			else
				NdotL = 1;
			// Use the diffuse binary calculation to determine the RGB value to use for the surface 
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
			// Pass through the alpha value
			c.a = s.Alpha;
			return c;
		}

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
