﻿using UnityEngine;
using System.Collections;

public class Utility {

    public static IEnumerator MoveOverSeconds(GameObject obj, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        // We are using built-in physics, so we will modify position via 'move-position
        Rigidbody rb = obj.GetComponent<Rigidbody>();
        Vector3 startingPos = obj.transform.position;
        while (elapsedTime < seconds)
        {
            rb.MovePosition(Vector3.Lerp(startingPos, end, Mathf.SmoothStep(0, 1, elapsedTime / seconds)));

            // Sync up with frame rate
            elapsedTime += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        rb.MovePosition(Vector3.Lerp(startingPos, end, Mathf.SmoothStep(0, 1, 1)));
    }

    public static IEnumerator ScaleOverSeconds(GameObject obj, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Transform trans = obj.transform;
        Vector3 startingScale = trans.localScale;
        while (elapsedTime < seconds)
        {
            trans.localScale = Vector3.Lerp(startingScale, end, Mathf.SmoothStep(0, 1, elapsedTime / seconds));

            // Sync up with frame rate
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        trans.localScale = Vector3.Lerp(startingScale, end, Mathf.SmoothStep(0, 1, 1));
    }

}
