﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public float x_speed = 10.0F;
    public float max_x_speed = 5.0F;
    public float max_x_sprint_speed = 8.0F;
    public float double_tap_time = 0.03F;
    public float double_tap_boost = 1.5F;
    public float jump_height = 50.0F;
    public float jump_wall_force = 25.0F;
    public int prolong_jump_frames = 60;
    public float bump_force= 1000.0F;
    public float ground_epsilon = 0.05F;
    public string bumper_tag;
    public string pickup_tag;
    public string obstacle_tag;
    public string finish_tag;
    public GameObject live_box;
    public GameManager gm;

    private Rigidbody m_rb;
    private Collider m_collider;
    private float distance_from_ground = 0.0F;
    private float distance_from_wall = 0.0F;
    private float get_axis_horizontal = 0.0F;
    private bool get_key_down_jump = false;
    private bool get_key_down_sprint = false;

    private DoubleTapDetector dtd_left;
    private DoubleTapDetector dtd_right;
    private int seq_num = 0;

    // TODO: Add a tiny delay after a player jumps in which they are _not_ grounded. This enables us to use 'getKey' instead of 'getKeyDown'
    // TODO: Use the collider based approach to isGrounded detection alongside the raytrace based approach.

    // Use this for initialization
    void Start () {
        m_rb = GetComponent<Rigidbody>();
        m_collider = GetComponent<Collider>();
        distance_from_ground = m_collider.bounds.extents.y;
        distance_from_wall = m_collider.bounds.extents.x;
        dtd_left = new DoubleTapDetector("a", double_tap_time);
        dtd_right = new DoubleTapDetector("d", double_tap_time);
    }

    // We use update to get inputs from the keyboard/player
    // this is the proper place to gather inputs to ensure that we don't drop any keystrokes which can happen in FixedUpdate
    void Update()
    {
        seq_num++;
        get_axis_horizontal = Input.GetAxis("Horizontal");
        get_key_down_jump = Input.GetButton("Jump");
        get_key_down_sprint = Input.GetButton("Sprint");
    }


    void FixedUpdate ()
    {
        if (get_key_down_jump && is_jump_enabled &&
            (isGrounded(Vector3.down) || isGrounded(Vector3.right) || isGrounded(Vector3.left)))
        { 
            disable_jump_until_end_of_frame();
            StartCoroutine(prolong_jump());
            m_rb.velocity = new Vector3(m_rb.velocity.x, 0, m_rb.velocity.z);
            // TODO -- Somehou
            Debug.Log(Time.realtimeSinceStartup + ": Jumping!");
            m_rb.AddForce(0.0F, jump_height, 0.0F);
                
            // If the wall is to our right, add a force to the left
            if (isGrounded(Vector3.right))
            {
                m_rb.velocity = new Vector3(0, m_rb.velocity.y, m_rb.velocity.z);
                m_rb.AddForce(-jump_wall_force, 0.0F, 0.0F);
            }

            // If the wall is to our left, add a force to the right
            if (isGrounded(Vector3.left))
            {
                m_rb.velocity = new Vector3(0, m_rb.velocity.y, m_rb.velocity.z);
                m_rb.AddForce(jump_wall_force, 0.0F, 0.0F);
            }
        }

        

        /*
        if (dtd_left.detectedDoubleTap())
        {
            // TODO: Do we want a 'smooth' boost or a 'dash' boost? Smooth: use moveHorizontal, Dash: modify velocity directly
            m_rb.velocity = new Vector3(m_rb.velocity.x-double_tap_boost, m_rb.velocity.y, m_rb.velocity.z);
            //moveHorizontal *= double_tap_boost;
        }
        if (dtd_right.detectedDoubleTap())
        {
            // TODO: Do we want a 'smooth' boost or a 'dash' boost? Smooth: use moveHorizontal, Dash: modify velocity directly
            m_rb.velocity = new Vector3(m_rb.velocity.x + double_tap_boost, m_rb.velocity.y, m_rb.velocity.z);
            //moveHorizontal *= double_tap_boost;
        }
        */
        float movement = get_axis_horizontal;
        m_rb.AddForce(new Vector3(movement * x_speed, 0.0F, 0.0F));
        float max_speed = get_key_down_sprint ? max_x_sprint_speed : max_x_speed;
        m_rb.velocity = new Vector3(Mathf.Clamp(m_rb.velocity.x, -max_speed, max_speed), m_rb.velocity.y, m_rb.velocity.z);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(pickup_tag))
        {
            other.gameObject.SetActive(false);
            gm.score++;
            Debug.Log("Player scored! " + gm.score);
        }
        if (other.gameObject.CompareTag(obstacle_tag))
        {
            // TODO: Make a Particle explosion effect
            if (!gm.mutex_lock)
            {
                game_over_effects();
                gm.game_over();
                Debug.Log("Player colided with obstacle... Game Over");
            }
        }
        if (other.gameObject.CompareTag(bumper_tag))
        {
            Vector3 enemy_position = other.gameObject.transform.parent.transform.position;
            m_rb.AddExplosionForce(bump_force,enemy_position, 30);
            Rigidbody o_rb = other.gameObject.GetComponentInParent<Rigidbody>();
            o_rb.AddExplosionForce(bump_force, transform.position, 30);
        }
        if (other.gameObject.CompareTag(finish_tag))
        {
            if (!gm.mutex_lock)
            {
                gm.level_finished();
                Debug.Log("Player finished the level!");
            }
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(live_box.tag))
        {
            // TODO: Make a particle explosion onto screen or make the ball bounce in some way?
            game_over_effects();
            gm.game_over();
        }
    }

    private bool is_jump_enabled = true;
    private void disable_jump_until_end_of_frame()
    {
        is_jump_enabled = false;
        StartCoroutine(enable_jump());
    }
    private IEnumerator enable_jump()
    {
        int number_of_frames_to_disable_jump = 5;
        for (int i = 0; i < number_of_frames_to_disable_jump; i++) 
            yield return new WaitForEndOfFrame();
        is_jump_enabled = true;
    }
    private IEnumerator prolong_jump()
    {
        for (int i = 0; i < prolong_jump_frames; i++)
        {
            if (get_key_down_jump)
            { 
                m_rb.AddForce(-Physics.gravity*prolong_jump_coefficient(i));
            }
            yield return new WaitForFixedUpdate();
        }
    }


    [Header("Prolonged Jump Physics")]
    public float b = 0.5f;
    public float y = 0.8f;
    public float m = 10.0f;
    private float prolong_jump_coefficient(int x)
    {
        return y + m * Mathf.Pow(b, x);
    }

    bool isGrounded()
    {
        return isGrounded(Vector3.down) || isGrounded(Vector3.left) || isGrounded(Vector3.right);
    }
    bool isGrounded(Vector3 direction)
    {
        int floor_layer = 1 << 8;
        bool ret = Physics.Raycast(transform.position, direction, distance_from_ground + ground_epsilon, floor_layer);
        if (ret)
            Debug.Log("Raycast down for ground check was true", gameObject);
        return ret;
    }

    void game_over_effects()
    {
        gameObject.SetActive(false);
    }
}

class DoubleTapDetector
{
    private float last_tap = 0.0F;
    private float thresh;
    private string key;
    public DoubleTapDetector(string _key, float double_tap_time)
    {
        this.thresh = double_tap_time;
        this.key = _key;
    }

    public bool detectedDoubleTap()
    {
        bool ret = false;
        if (Input.GetKeyDown(key)) {
            // If the key was pressed within the threshold from the last time it 
            // was pressed, we have successfully detected a double tap 
            if (Time.time - last_tap < thresh)
            {
                Debug.Log("Double Tap detected for key " + key);
                ret = true;
            }
            // Whenever the key is pressed we want to update our 'last_tap' time
            last_tap = Time.time;
        }
        return ret;
    }
}
